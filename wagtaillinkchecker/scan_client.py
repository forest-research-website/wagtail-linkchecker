import os
import logging
import requests
from django.utils.translation import ugettext_lazy as _
from wagtaillinkchecker import HTTP_STATUS_CODES
from url_normalize import url_normalize

logger = logging.getLogger(__name__)

# user agent string from environment, used to prevent 403 responses from requests
user_agent = os.getenv('LINK_USER_AGENT', 'Mozilla/5.0')

def get_url(url, page, site, use_head, stream_content):
    data = {
        'url': url,
        'page': page,
        'site': site,
        'error': False,
        'invalid_url': False
    }
    response = None
    try:
        headers = {
            'User-Agent': user_agent
        }
        # the timeouts should really be configurable, in the settings maybe?
        connect_timeout, read_timeout = 10.0, 60.0
        if use_head:
            logger.debug('Checking link %s using HEAD', url)
            response = requests.head(url, verify=True, headers=headers, timeout=(connect_timeout, read_timeout))
        else:
            logger.debug('Checking link %s using GET and stream: %s', url, stream_content)
            response = requests.get(url, verify=True, headers=headers, timeout=(connect_timeout, read_timeout), stream=stream_content)
        data['response'] = response
    except UnicodeError:
        data['invalid_url'] = True
        data['error_message'] = _('The link contained an invalid unicode character or sequence')
        return data
    except (requests.exceptions.InvalidSchema, requests.exceptions.MissingSchema, requests.exceptions.InvalidURL):
        data['invalid_url'] = True
        return data
    except requests.exceptions.ConnectionError as ex:
        data['error'] = True
        data['error_message'] = _('There was an error connecting to this site')
        return data
    except requests.exceptions.RequestException as ex:
        data['error'] = True

        if response and response.status_code:
            data['status_code'] = response.status_code

        data['error_message'] = type(ex).__name__ + ': ' + str(ex)
        return data

    else:
        if response.status_code not in range(100, 400):
            error_message_for_status_code = HTTP_STATUS_CODES.get(response.status_code)
            data['error'] = True
            data['status_code'] = response.status_code
            if error_message_for_status_code:
                data['error_message'] = error_message_for_status_code[0]
            else:
                if response.status_code in range(400, 500):
                    data['error_message'] = 'Client error'
                elif response.status_code in range(500, 600):
                    data['error_message'] = 'Server Error'
                else:
                    data['error_message'] = "Error: Unknown HTTP Status Code '{0}'".format(response.status_code)
        return data


def clean_url(url, site, current_url):

    if not url:
        return None

    # ignore fragments
    if url.startswith('#'):
        return None

    # resolve absolute URLs
    if url.startswith('/'):
        return site.root_url + url

    # resolve relative URLs
    if url.startswith('./') or url.startswith('../'):
        if not current_url.endswith('/'):
            return url_normalize(current_url + '/' + url)
        return url_normalize(current_url + url)

    # accept http/https URLs
    url_lc = url.lower()
    if url_lc.startswith("http://"):
        return url
    if url_lc.startswith("https://"):
        return url

    return None
