from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.db.models.signals import pre_delete
from django.db.models import Q
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _

from wagtail.core.models import Site
from wagtail.core.models import Page


class SitePreferences(models.Model):
    site = models.OneToOneField(
        Site, unique=True, db_index=True, editable=False, on_delete=models.CASCADE)
    automated_scanning = models.BooleanField(
        default=False,
        help_text=_(
            'Conduct automated sitewide scans for broken links, and send emails if a problem is found.'),
        verbose_name=_('Automated Scanning')
    )
    link_scan_limit = models.IntegerField(
        default=50000,
        help_text=_(
            'Approximate number of links to crawl before concluding the scan. Use a high amount if a full scan ' + 
            'is required (e.g. 100000), only use a lower value for testing. Note the number of crawled links may ' + 
            'vary to this number due to concurrency when performing the link check.'
        ),
        verbose_name=_('Scan Link Limit')
    )
    scans_to_keep = models.IntegerField(
        default=3,
        help_text=_(
            'The number of previous scans other than a newly started one to keep at a given time. When starting a ' +
            'new scan, the oldest scans will be deleted until this number is hit.'
        ),
        verbose_name=_('Scans To Keep')
    )
    excluded_domains = models.CharField(
        blank=True,
        null=True,
        max_length=1000,
        help_text=_(
            'Comma separated list of domains to ignore whilst scanning. These could be domains that regularly cause ' +
            'issues whilst scanning or domains that regularly end up as broken when they appear to be okay. ' +
            'e.g. test.com,test2.com'
        ),
        verbose_name=_('Excluded Domains')
    )
    slowly_requested_domains = models.CharField(
        blank=True,
        null=True,
        max_length=1000,
        help_text=_(
            'Comma separated list of domains to wait some time before hitting with multiple requests. For example ' +
            'if a domain is regularly returning 429 (too many requests) responses a break between requests could ' +
            'help prevent these responses.'
        ),
        verbose_name=_('Slowly Requested Domains')
    )
    request_wait_time = models.PositiveIntegerField(
        default=3,
        validators=[
            MinValueValidator(1),
            MaxValueValidator(60)
        ],
        help_text=_(
            'Wait time for the slowly requested domains. The link checker will hang for this time between requests ' +
            'to the domains in the slowly requested domains list.'
        ),
        verbose_name=_('Wait Time')
    )


class ScanQuerySet(models.QuerySet):
    def finished_ordered_by_scan_started(self, site):
        return self.filter(site=site).filter(scan_finished__isnull=False).order_by('-scan_started')


class Scan(models.Model):
    scan_finished = models.DateTimeField(blank=True, null=True)
    scan_started = models.DateTimeField(auto_now_add=True)
    site = models.ForeignKey(
        Site, db_index=True, editable=False, on_delete=models.CASCADE)

    objects = ScanQuerySet.as_manager()

    @property
    def is_finished(self):
        return self.scan_finished is not None

    def add_link(self, url=None, page=None):
        return ScanLink.objects.create(scan=self, url=url, page=page)

    def result(self):
        return _('{0} broken links found out of {1} links'.format(self.broken_links.count(), self.links.count()))

    def __str__(self):
        return 'Scan - {0}'.format(self.scan_started.strftime('%d/%m/%Y'))


class ScanLinkQuerySet(models.QuerySet):

    def valid(self):
        return self.filter(invalid=False)

    def invalid_links(self):
        return self.filter(invalid=True)

    def crawled_links(self):
        return self.valid().filter(crawled=True)

    def non_crawled_links(self):
        return self.valid().filter(crawled=False)

    def broken_links(self):
        return self.valid().filter(broken=True)

    def broken_or_invalid_links(self):
        return self.filter(Q(broken=True) | Q(invalid=True))

    def working_links(self):
        return self.valid().filter(broken=False, crawled=True)

    def broken_links_sorted_by_primary_contact(self):
        """find all broken links and sort using the primary_contact field"""
        return self.valid().filter(broken=True).order_by('primary_contact')

    def broken_links_with_owners(self):
        """find the entries marked as broken that are on pages with an owner field set"""
        # use Djangos __ ORM syntax so we get a JOIN query
        return self.valid().filter(broken=True).select_related(
            'page').filter(page__owner__isnull=False)

    def broken_or_invalid_links_with_owners(self):
        """find the entries marked as broken that are on pages with an owner field set"""
        # use Djangos __ ORM syntax so we get a JOIN query
        return self.filter(Q(broken=True) | Q(invalid=True)).select_related(
            'page').filter(page__owner__isnull=False)

    def broken_links_without_owners(self):
        """find the entries marked as broken that are on pages with no owner field set"""
        # use Djangos __ ORM syntax so we get a JOIN query
        return self.valid().filter(broken=True).select_related(
            'page').filter(page__owner__isnull=True)

    def broken_or_invalid_links_without_owners(self):
        """find the entries marked as broken that are on pages with no owner field set"""
        # use Djangos __ ORM syntax so we get a JOIN query
        return self.filter(Q(broken=True) | Q(invalid=True)).select_related(
            'page').filter(page__owner__isnull=True)


class ScanLink(models.Model):
    scan = models.ForeignKey(Scan, related_name='links',
                             on_delete=models.CASCADE)
    url = models.URLField(max_length=2048)

    # If the link has been crawled
    crawled = models.BooleanField(default=False)

    # Link is not necessarily broken, it is invalid (eg a tel link or not an actual url)
    invalid = models.BooleanField(default=False)

    # If the link is broken or not
    broken = models.BooleanField(default=False)

    # Error returned from link, if it is broken
    status_code = models.IntegerField(blank=True, null=True)
    error_text = models.TextField(blank=True, null=True)

    # Page where link was found
    page = models.ForeignKey(Page, null=True, on_delete=models.SET_NULL)

    # Page this link was on was deleted
    page_deleted = models.BooleanField(default=False)

    page_slug = models.CharField(max_length=512, null=True, blank=True)

    # Available contact information for the page/link, this can be either the page owner
    # if they are assigned, or any other contact information that we're able to retrieve.
    # This column will be populated through an active scan
    contact_information = models.TextField(blank=True, null=True)

    # Primary contact information for the page/link. This will be selected based on a hierarchy
    # page owner -> first research group lead staff member (if applicable) -> lead author (if applicable)
    # -> first related contact. This is for use when sorting on the results page of the link checker
    primary_contact = models.CharField(max_length=512, null=True, blank=True)

    objects = ScanLinkQuerySet.as_manager()

    class Meta:
        unique_together = [('url', 'scan')]

    def __str__(self):
        return self.url

    def has_owner(self):
        return True if self.page.owner else False

    @property
    def page_is_deleted(self):
        return self.page_deleted and self.page_slug

    def check_link(self):
        from wagtaillinkchecker.tasks import check_link
        check_link.apply_async((self.pk, ))


@receiver(pre_delete, sender=Page)
def delete_tag(instance, **kwargs):
    ScanLink.objects.filter(page=instance).update(
        page_deleted=True, page_slug=instance.slug)
