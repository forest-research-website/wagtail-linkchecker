# Generated by Django 2.0.13 on 2020-09-11 11:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('wagtaillinkchecker', '0010_sitepreferences_scans_to_keep'),
    ]

    operations = [
        migrations.AddField(
            model_name='sitepreferences',
            name='excluded_domains',
            field=models.CharField(help_text='Comma separated list of domains to ignore whilst scanning. These could be domains that regularly cause issues whilst scanning or domains that regularly end up as broken when they appear to be okay. e.g. test.com,test2.com', max_length=1000, null=True, verbose_name='Excluded Domains'),
        ),
    ]
