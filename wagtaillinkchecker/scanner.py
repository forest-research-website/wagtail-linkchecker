try:
    from http import client as http_client
except ImportError:
    import httplib as http_client

import logging
from django.utils.translation import ugettext_lazy as _
from wagtaillinkchecker.models import Scan
from wagtaillinkchecker.tasks import start_scan

logger = logging.getLogger(__name__)

def get_celery_worker_status():
    ERROR_KEY = "ERROR"
    try:
        from celery.task.control import inspect
        insp = inspect()
        d = insp.stats()
        if not d:
            d = {ERROR_KEY: 'No running Celery workers were found.'}
    except IOError as e:
        from errno import errorcode
        msg = "Error connecting to the backend: " + str(e)
        if len(e.args) > 0 and errorcode.get(e.args[0]) == 'ECONNREFUSED':
            msg += ' Check that the RabbitMQ server is running.'
        d = {ERROR_KEY: msg}
    except ImportError as e:
        d = {ERROR_KEY: str(e)}
    return d


class Link(Exception):

    def __init__(self, url, page, status_code=None, error=None, site=None):
        self.url = url
        self.status_code = status_code
        self.error = error
        self.site = site
        self.page = page

    @property
    def message(self):
        if self.error:
            return self.error
        elif self.status_code in range(100, 300):
            message = "Success"
        elif self.status_code in range(500, 600) and self.url.startswith(self.site.root_url):
            message = str(self.status_code) + ': ' + _('Internal server error, please notify the site administrator.')
        else:
            try:
                message = str(self.status_code) + ': ' + http_client.responses[self.status_code] + '.'
            except KeyError:
                message = str(self.status_code) + ': ' + _('Unknown error.')
        return message

    def __str__(self):
        return self.url

    def __eq__(self, other):
        if not isinstance(other, Link):
            return NotImplemented
        return self.url == other.url

    def __hash__(self):
        return hash(self.url)


def broken_link_scan(site):
    logger.debug('Starting scan for site %s', site)
    scan = Scan.objects.create(site=site)
    start_scan.apply_async((scan.pk, ))
    return scan
