import os
import logging
from celery import shared_task
from bs4 import BeautifulSoup
from django.utils.translation import ugettext_lazy as _

from django.db.utils import IntegrityError
from django.utils import timezone

import tldextract
import time

from wagtaillinkchecker.scan_client import get_url, clean_url
from wagtaillinkchecker.models import Scan, ScanLink, SitePreferences

from research.models import ResearchTopicPage, PublicationPage
from services.models import ServicePage
from staff.models import StaffMemberPage
from tools_and_resources.models import ToolsAndResourcesPage

logger = logging.getLogger(__name__)

# these are really only for debugging/development and should really be removed
page_scan_offset = int(os.getenv('PAGE_SCAN_OFFSET', '0'))
page_scan_length = int(os.getenv('PAGE_SCAN_LENGTH', '100000'))

# ideally this should be a SitePreference
use_head_for_same_site_links = bool(os.getenv('USE_HEAD_FOR_SAME_SITE_LINKS', 'True'))


@shared_task
def start_scan(scan_pk):
    """Start a scan by finding all pages"""

    try:
        scan = Scan.objects.get(pk=scan_pk)
        site = scan.site

        # first clean up old scans
        __housekeep_scans(site)

        site_preferences = SitePreferences.objects.filter(site=site).first()
        link_scan_limit = site_preferences.link_scan_limit

        page_scan_upper = page_scan_offset + min(link_scan_limit, page_scan_length)

        pages = site.root_page.get_descendants(inclusive=True).live().public()
        num_site_pages = len(pages)
        pages = pages[page_scan_offset:page_scan_upper]
        logger.info('Starting scan. Site contains %d pages, applying limits of page_scan_offset: %d, page_scan_length: %d and link_scan_limit: %d so will scan pages %d:%d and will check %d max links',
                     num_site_pages, page_scan_offset, page_scan_length, link_scan_limit, page_scan_offset, page_scan_upper, link_scan_limit)

        __scan_pages(scan, pages)
    except Scan.DoesNotExist:
        logger.exception(
            "Unable to start scan as the scan %s could not be found")


def __housekeep_scans(site):
    """Remove old scans. Only keeps last n completed scans."""
    preferences = SitePreferences.objects.filter(site=site).first()
    scans_to_keep = preferences.scans_to_keep
    scans = Scan.objects.finished_ordered_by_scan_started(site=site)
    scans = scans[scans_to_keep:]
    for scan in scans:
        logger.info("Deleting (housekeeping) scan %s", scan)
        scan.delete()


def __scan_pages(scan, pages):
    """Scan the given pages adding to the db table first, then creating tasks for each entry"""
    logger.debug('Scanning %d pages on site %s', len(pages), scan.site)

    logger.debug('Adding %s page links to database', len(pages))
    links = []
    for page in pages:
        try:
            ScanLink.objects.get(url=page.full_url, scan=scan)
            return
        except ScanLink.DoesNotExist:
            link = ScanLink.objects.create(
                url=page.full_url, page=page, scan=scan)
            links.append(link)

    logger.debug(
        'Pushing %s page links to task queue for scanning', len(links))
    for link in links:
        link.check_link()


@shared_task
def check_link(link_pk):
    """Check link availabilty by GETting the link"""
    url = None
    try:
        link = ScanLink.objects.get(pk=link_pk)
        site = link.scan.site
        site_preferences = SitePreferences.objects.filter(site=site).first()
        link_scan_limit = site_preferences.link_scan_limit

        logger.info('Checking link URL: %s', link.url)

        if link.scan.is_finished:
            logger.debug('Scan %s is marked as complete so ignoring link pk: %s, URL: %s', link.scan, link_pk, link.url)
            return

        # mark the link as crawled at the start of the task so we keep the
        # queded task execution and task counting aligned and in sync
        link.crawled = True
        link.save()

        # if scanning our own website and its not a page (where we need the content
        # in order to extract the links) we know HEAD works so use it in preference
        # to GET
        #
        # (pillingworth, 2020-09-08) add a setting, "Use HEAD for internal links"
        # which will be combined with the following logic so can explicitly enable
        # HEAD as an optomisation (and turn it off if causing issues)
        is_same_site = link.url.startswith(site.root_url)
        is_page = (link.page.full_url == link.url)
        use_head = use_head_for_same_site_links and is_same_site and not is_page
        logger.debug('Link url: %s, site.root_url: %s, use_head_for_same_site_links: %s, is_same_site: %s, ' +
                     'is_page: %s, use_head: %s', link.url, site.root_url, use_head_for_same_site_links, is_same_site,
                     is_page, use_head)

        # if url is one of the excluded domains, don't check
        domain_excluded = False if is_same_site else is_domain_excluded(site_preferences, link.url)

        if domain_excluded is True:
            logger.debug(f'Domain excluded: {link.url}')
            link.broken = False
            link.error_text = 'Excluded link: not checked'
            link.save()
        else:
            if not is_same_site and is_domain_wait_required(site_preferences, link.url):
                logger.debug('Wait required for %s seconds...', site_preferences.request_wait_time)
                time.sleep(int(site_preferences.request_wait_time))

            url = get_url(link.url, link.page, site, use_head, True)
            link.status_code = url.get('status_code')
            logger.info('Check link result: %s', url)

            if url['error']:
                link.broken = True
                link.error_text = url['error_message']

                contacts = get_contact_information(link)
                link.contact_information = contacts.get('contact_information')
                link.primary_contact = contacts.get('primary_contact')

            elif url['invalid_url']:
                link.invalid = True
                if url.get('error_message'):
                    link.error_text = url.get('error_message')
                else:
                    link.error_text = _('Link was invalid')

            elif link.page.full_url == link.url:
                content_type = url['response'].headers.get('Content-Type')
                logger.debug(
                    'URL %s is an internal page with content-type %s so parsing HTML content', link.url, content_type)
                soup = BeautifulSoup(url['response'].content, 'html5lib')

                anchors = soup.find_all('a')
                logger.debug('Found %s anchor tags', len(anchors))

                images = soup.find_all('img')
                logger.debug('Found %s image tags', len(images))

                for anchor in anchors:
                    raw_link_href = anchor.get('href')
                    link_href = clean_url(raw_link_href, site, link.url)
                    if link_href:
                        try:
                            logger.debug(
                                'Adding anchor URL %s to queue', link_href)
                            new_link = link.scan.add_link(
                                page=link.page, url=link_href)
                            new_link.check_link()
                        except IntegrityError:
                            # assumption is failed to add because unique constraint on scan/url has failed as scan/URL is already in database
                            logger.debug(
                                'Failed to add anchor URL %s to queue due to a db integrity error. Assuming URL is aleady present to be scanned.', link_href)
                    else:
                        logger.debug(
                            'Ignoring anchor URL %s as not a scannable URL', raw_link_href)

                for image in images:
                    raw_image_src = image.get('src')
                    image_src = clean_url(raw_image_src, site, link.url)
                    if image_src:
                        try:
                            logger.debug('Adding image URL %s to queue', image_src)
                            new_link = link.scan.add_link(
                                page=link.page, url=image_src)
                            new_link.check_link()
                        except IntegrityError:
                            # assumption is failed to add because unique constraint on scan/url has failed as scan/URL is already in database
                            logger.debug(
                                'Failed to add image URL %s to queue due to a db integrity error. Assume URL is aleady present to be scanned.', link_href)
                    else:
                        logger.debug(
                            'Ignoring image URL %s as not a scannable URL', raw_link_href)

        # make sure we update status_code and any errors etc
        link.save()

        # the scan is finished when there are no more links in the database with the crawled flag set to false
        # or the number of links scanned exceeds the maximum to be scanned
        non_crawled_links = link.scan.links.non_crawled_links().exists()
        crawled_links_count = link.scan.links.crawled_links().count()
        finished_scanning = not non_crawled_links or crawled_links_count >= link_scan_limit
        logger.debug('unscanned_links: %s, scanned_links_count: %s, finished_scanning: %s',
                     non_crawled_links, crawled_links_count, finished_scanning)
        if finished_scanning:
            scan = link.scan
            scan.scan_finished = timezone.now()
            scan.save()
            logger.info('Scan completed at %s', scan.scan_finished)

    except ScanLink.DoesNotExist:
        # if scan has been deleted then log and consume exception as this is an expected exception
        logger.warning(
            'Unable to find database entry for link %s, so ignoring', link_pk)

    except Exception as ex:
        # catch unexpected exceptions, log and re-throw so celery can handle failed tasks if so configured
        logger.exception('Unexpected exception checking link %s', link_pk)
        raise ex

    finally:
        logger.debug('Completed checking link %s', link_pk)

        # get_url() returns the response and so we are responsible for closing (important when using stream=True)
        if url is not None and url.get('response') is not None:
            logger.debug('Closing response')
            url.get('response').close()


def get_contact_information(link):
    """Gets a form of contact information for a given link if available"""
    contact_information = ''
    primary_contact = None

    if link.page.owner is not None:
        logger.debug(f'Page has an owner: {link.page.owner.email}')
        contact_information += 'Page owner: ' + link.page.owner.email
        primary_contact = link.page.owner.email

    page_content_type = link.page.content_type
    page_model_class = str(page_content_type.model_class())

    logger.info(f'Page Content Type: {page_content_type.name}')
    logger.info(f'Page Model Class: {page_model_class}')

    # if research topic get research group authors for page then staff member contacts
    if 'ResearchTopicPage' in page_model_class:
        rtp = ResearchTopicPage.objects.filter(page_ptr_id=link.page.id).distinct().first()
        research_groups = rtp.research_groups

        if research_groups:
            logger.debug(f'Research groups: {research_groups}')

            for i, rg in enumerate(research_groups):
                if rg.lead_staff_member is not None:
                    if i == 0:
                        contact_information += ' Research group(s) lead staff: '

                        if primary_contact is None:
                            primary_contact = rg.lead_staff_member.email
                    else:
                        contact_information += ', '

                    contact_information += rg.lead_staff_member.email

        # Add related contacts to contact information string
        contact_information += get_contacts(rtp.contacts)
        if primary_contact is None and len(rtp.contacts) > 0:
            primary_contact = rtp.contacts[0].email

    # if publication page get lead author if exists, if not get default related contacts
    elif 'PublicationPage' in page_model_class:
        pp = PublicationPage.objects.filter(page_ptr_id=link.page.id).distinct().first()
        lead_author = pp.lead_author

        if lead_author is not None and lead_author.staff_member is not None:
            logger.info(f'Publication Page lead author: {lead_author.staff_member.email}')
            contact_information += ' Lead author: ' + lead_author.staff_member.email

            if primary_contact is None:
                primary_contact = lead_author.staff_member.email

        contact_information += get_contacts(pp.contacts)

    # if tools and resources page get default related contacts
    elif 'ToolsAndResourcesPage' in page_model_class:
        trp = ToolsAndResourcesPage.objects.filter(page_ptr_id=link.page.id).distinct().first()
        contact_information += get_contacts(trp.contacts)

        if primary_contact is None and len(trp.contacts) > 0:
            primary_contact = trp.contacts[0].email

    # if service page get default related contacts
    elif 'ServicePage' in page_model_class:
        sp = ServicePage.objects.filter(page_ptr_id=link.page.id).distinct().first()
        contact_information += get_contacts(sp.contacts)

        if primary_contact is None and len(sp.contacts) > 0:
            primary_contact = sp.contacts[0].email

    # if staff member page get the actual staff member
    elif 'StaffMemberPage' in page_model_class:
        smp = StaffMemberPage.objects.filter(page_ptr_id=link.page.id).distinct().first()
        logger.info(f'Staff member page email: {smp.email}')
        contact_information += ' Staff member email: ' + smp.email

        if primary_contact is None:
            primary_contact = smp.email

    # Tidy up contact information string
    contact_information = None if contact_information.strip() == '' else contact_information

    return {
        'contact_information': contact_information,
        'primary_contact': primary_contact
    }


def get_contacts(list_of_contacts):
    """Joins a list of class ContactItem contacts and joins into a string using the email"""
    return ' Related contacts: ' + (', '.join(c.email for c in list_of_contacts)) if len(list_of_contacts) > 0 else ''


def is_domain_excluded(site_preferences, url):
    """Checks if the url to be scanned is in the excluded domains list in the settings"""
    logger.debug('Checking to see if external domain is excluded...')
    return check_domain_matches(site_preferences.excluded_domains, url)


def is_domain_wait_required(site_preferences, url):
    """Checks if the url to be scanned is in the slow request domains list in the settings"""
    logger.debug('Checking to see if external domain is a slow requested domain...')
    return check_domain_matches(site_preferences.slowly_requested_domains, url)


def check_domain_matches(domains, url):
    if domains is not None:
        domain_list = [s.strip() for s in domains.lower().split(',')]
        extract = tldextract.extract(url)
        domain = f'{extract.domain}.{extract.suffix}'
        return any(domain.lower() == s for s in domain_list)
    else:
        return False
